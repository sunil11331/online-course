import React from 'react';
import { useState } from 'react';
import '../App.css';
import Index from './Index';
import Menu from './Service';

const Booking = () => {

    const [item, setItem] = useState(Menu);
  return (
      <div className='content'>
        <div className='container'>
          <div className='continue-booking'>
            <p>Time Left: 60 seconds</p>
            <h1>Claim Your Free Trial Class</h1>
          </div>
          <div className='class-schedule'>
            <h1>Class Schedule</h1>
            <p>Free Seats Left: <span style={{color:"orangered"}}>7</span></p>
          </div>
        <section className='box'>
            <div className='heading'>
                <p>Date</p>
                <p>Time</p>
                <p>Availability</p>
            </div>
            {
                item.map((curItem) =>{
                    return <Index key={curItem.id} {...curItem}/>
                })
            }
            
        </section>

    </div>
    </div>
  )
}

export default Booking