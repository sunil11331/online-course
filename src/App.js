import React from 'react';
import './App.css';
import Booking from './components/Booking';
import Intro from './components/Intro';
import Freeseats from './components/Freeseats';

function App() {
  return (
    <div className="App">
      <Intro/>
      <Booking/>
      <Freeseats/>
    </div>
  );
}

export default App;
